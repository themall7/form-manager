<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormSubmitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_submit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->bigInteger('form_id')->unsigned();
            $table->foreign('form_id')->references('id')->on('forms');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::disableForeignKeyConstraints();
        Schema::table('form_data', function(Blueprint $table) {
            $table->bigInteger('form_submit_id')->unsigned()->after('id');
            $table->foreign('form_submit_id')->references('id')->on('form_submit');
            $table->dropForeign('form_data_form_id_foreign');
            $table->dropForeign('form_data_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('form_id');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_submit');

        Schema::disableForeignKeyConstraints();
        Schema::table('form_data', function(Blueprint $table) {
            $table->dropForeign('form_data_form_id_foreign');
            $table->dropColumn('form_submit_id');
            $table->bigInteger('user_id')->unsigned()->after('id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->bigInteger('form_id')->unsigned()->after('user_id');
            $table->foreign('form_id')->references('id')->on('forms');
        });
        Schema::enableForeignKeyConstraints();
    }
}
