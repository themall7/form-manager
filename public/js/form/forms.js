$(document).ready(function(){
    init();
});

function init()
{
    load_data();
}

function display_data(data)
{
    $("#form_table tbody").empty();
    $.each(data, function(index, row){
        console.log(row);
        var form_name = row.name == null ? '' : row.name;
        var btn_view = '<a href="#;" class="btn btn-xs btn-default btn_view" data-id="'+row.id+'"><i class="fa fa-eye"></i> View</a>';
        var tr = '<tr><td>'+form_name+'</td><td>'+btn_view+'</td></tr>';
        $("#form_table tbody").append(tr);
    });
    $('.btn_view').click(function(){
        location.href = '/forms/' + $(this).data('id');
    });
}

function load_data()
{
    $.ajax({
        type: 'get',
        url: "/api/forms",
        data: '',
        success: function(result){
            console.log(result);
            if (result.success) {
                display_data(result.data);
            }
        },
        dataType: "json",
        contentType: "application/json",
    });
}
