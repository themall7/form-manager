$(document).ready(function(){
    init();
});

function init()
{
    load_data();

    $('#btn_save').click(function(){
        console.log(new FormData(document.getElementById('form_rendor')));
        var data = new FormData($('#form_rendor')[0]);
        console.log(data);
        $.ajax({
            type: 'post',
            url: "/api/forms/"+get_id(),
            data: (data),
            success: function(result){
                alert('saved');
            },
            dataType: "json",
            contentType: false,
            processData: false,
        });
    });
}

function add_element(row)
{
    var $elm_list = $('#elm_list');
    var element = '';

    if (row.type==='text') {
        element = '<input type="'+row.type+'" name="'+row.name+'" value="" class="form-control" id="'+row.name+'">';
    } else if (row.type==='textarea') {
        element = '<'+row.type+' name="'+row.name+'" class="form-control" id="'+row.name+'"></'+row.type+'>';
    } else if (row.type==='file') {
        element = '<input type="'+row.type+'" name="'+row.name+'" value="" class="form-control" id="'+row.name+'">';
    } else if (row.type==='select') {
        element = '<'+row.type+' name="'+row.name+'" class="form-control" id="'+row.name+'"></'+row.type+'>';
    }

    var form_group = `
        <div class="form-group col-md-12">
          <label>`+row.label+`</label>
          `+element+`
        </div>
    `;
    $elm_list.append(form_group);

    if (row.type==='select' && row.options!==null) {
        var options = row.options.split(/\r?\n/);
        $.each(options, function(index, option){
            console.log(option);
            $('#'+row.name).append($('<option>', {
                'value': option,
                'text': option.ucfirst(),
            }));
        });
    }
}

function display_data(data)
{
    $('#form_name').text(data.name);
    var element = data.element;

    //$elm_list.html("");
    $.each(element, function(index, row){
        console.log(row);
        add_element(row);
    });
}

function load_data()
{
    $.ajax({
        type: 'get',
        url: "/api/forms/"+get_id(),
        data: '',
        success: function(result){
            console.log(result);
            if (result.success) {
                display_data(result.data);
            }
        },
        dataType: "json",
        contentType: "application/json",
    });
}

function get_id()
{
    var url = location.href;
    var param = url.split('/');
    return param.pop();
}

String.prototype.ucfirst = function()
{
    return this.charAt(0).toUpperCase() + this.substr(1);
}