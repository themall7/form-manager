$(document).ready(function(){
    var key = 'access_token';
    var access_token = $.cookie(key);
    if (access_token) {
        localStorage.setItem(key, access_token);

        $.removeCookie(key, { path: '/' });
    }
});

(function ($) {
    $.extend({
        getKey: function () {
            return localStorage.getItem('access_token');
        }
    });
})(jQuery);

$.ajaxSetup({
    headers: {'Authorization': 'Bearer '+$.getKey()}
});

function setHeader(request)
{
    request.setRequestHeader('Authorization', getKey());
}