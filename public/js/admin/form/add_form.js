$(document).ready(function(){
    init();
});

function init()
{
    $('#btn_text').click(function(){
        add_elelemt('text');
    });

    $('#btn_textarea').click(function(){
        add_elelemt('textarea');
    });

    $('#btn_dropdown').click(function(){
        add_elelemt('select');
    });

    $('#btn_file').click(function(){
        add_elelemt('file');
    });

    $('#elm_list').change(function(){
        edit_elelemt();
    });

    $('#elm_type').change(function(){
        change_element('type', $(this).val());
    });
    $('#elm_name').change(function(){
        change_element('name', $(this).val());
    });
    $('#elm_label').change(function(){
        change_element('label', $(this).val());
    });
    $('#elm_options').change(function(){
        change_element('options', $(this).val());
    });

    $('#btn_save').click(function(){
        var data = get_data();
        console.log(data);
        $.ajax({
            type: 'post',
            url: "/api/forms",
            data: JSON.stringify(data),
            success: function(result){
                alert('saved');
            },
            dataType: "json",
            contentType: "application/json",
        });
    });
}

function change_element(prop, new_value)
{
    var $option = $('#elm_list').children("option:selected");
    switch(prop) {
        case 'type' :
            $option.data(prop, new_value);
            break;
        case 'name' :
            $option.val(new_value);
            break;
        case 'label' :
            $('#elm_list').children("option:selected").text(new_value);
            break;
        case 'options' :
            $('#elm_list').children("option:selected").data(prop, new_value);
            break;
    }
}

function get_data()
{
    var items = $("#elm_list > option").map(function() {
        var arr = [];

        var elm = new Object();
        elm.type = $(this).data('type');
        elm.name = $(this).val();
        elm.label = $(this).html();
        elm.options = $(this).data('options');

        if (elm.type !== undefined) arr.push(elm);
        return arr;
    }).get();

    var form_name = $('#form_name').val();
    return {'form_name':form_name, 'element':items};
}

function add_elelemt(type)
{
    var len = $('#elm_list > option').length;
    var $elm_list = $('#elm_list');
    var value = type + '_' + (len+1);
    $elm_list.append($('<option>', {
        'value': value,
        'text': type.ucfirst() + ' ' + (len+1),
        'data-type': type,
        'data-options': '',
    }));
    $elm_list.val(value);
    edit_elelemt();
}

function edit_elelemt()
{
    var $elm_list = $('#elm_list');
    var $option = $elm_list.children("option:selected");
    var type = $option.data('type');
    $('#elm_type').val(type);
    $('#elm_name').val($elm_list.val());
    $('#elm_label').val($option.html());
    $('#elm_options').val($option.data('options'));
    $('#elm_options').prop('readonly', type!=='select');
}

String.prototype.ucfirst = function()
{
    return this.charAt(0).toUpperCase() + this.substr(1);
}