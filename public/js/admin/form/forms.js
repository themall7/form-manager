$(document).ready(function(){
    init();
});

function init()
{
    load_data();
}

function display_data(data)
{
    $("#form_table tbody").empty();
    $.each(data, function(index, row){
        console.log(row);
        var form_name = row.name == null ? '' : row.name;
        var btn_edit = '<a href="#;" class="btn btn-xs btn-default btn_edit" data-id="'+row.id+'"><i class="fa fa-edit"></i> Edit</a>';
        var btn_delete = '<a href="#;" class="btn btn-xs btn-default btn_delete" data-id="'+row.id+'"><i class="fa fa-trash"></i> Delete</a>';
        var tr = '<tr><td>'+form_name+'</td><td>'+btn_edit+btn_delete+'</td></tr>';
        $("#form_table tbody").append(tr);
    });
    $('.btn_edit').click(function(){
        location.href = '/admin/forms/edit/' + $(this).data('id');
    });
    $('.btn_delete').click(function(){
        delete_data($(this).data('id'));
    });
}

function load_data()
{
    $.ajax({
        type: 'get',
        url: "/api/forms",
        data: '',
        success: function(result){
            console.log(result);
            if (result.success) {
                display_data(result.data);
            }
        },
        dataType: "json",
        contentType: "application/json",
    });
}

function delete_data(id)
{
    $.ajax({
        type: 'delete',
        url: "/api/forms/"+id,
        data: '',
        success: function(result){
            console.log(result);
            if (result.success) {
                load_data();
            }
        },
        dataType: "json",
        contentType: "application/json",
    });
}
