<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api', 'namespace' => 'Api\Form', 'prefix' => 'forms'], function () {
    Route::get('/', 'FormController@getForms');
    Route::get('/{id}', 'FormController@getForm');
    Route::post('/', 'FormController@postForm');
    Route::put('/{id}', 'FormController@putForm');
    Route::delete('/{id}', 'FormController@deleteForm');
    Route::post('/{id}', 'FormController@postSubmitForm');
});