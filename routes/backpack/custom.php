<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

// admin routes
Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () {
    Route::get('/forms','FormController@viewForms')->name('view_forms');
    Route::get('/forms/add','FormController@addForm')->name('add_form');
    Route::get('/forms/edit/{id}','FormController@editForm')->name('edit_form');
});

// user routes
Route::group([
    'prefix'     => '',
    'middleware' => ['web'],
    'namespace'  => 'App\Http\Controllers',
], function () {
    Route::get('/forms','FormController@viewForms')->name('view_forms');
    Route::get('/forms/{id}','FormController@viewForm')->name('view_form');
});
