<?php

namespace App\Traits;

use stdClass;

trait UtilTrait
{
    public function getHash()
    {
        $hashed = md5(microtime());//Hash::make($key);
        $digit12 = substr($hashed, 0, 2);
        $digit34 = substr($hashed, 2, 2);

        return [$digit12, $digit34, $hashed];
    }
}
