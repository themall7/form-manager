<?php

namespace App\Traits\Models;

trait BaseModelTrait
{
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $common_fillable = ['created_at', 'updated_at']; // deleted_at

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $common_hidden = ['deleted_at'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $common_casts = [
        'created_at' => 'string',
        'updated_at' => 'string',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $common_dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $common_appends = [];

    public function init()
    {
        $this->fillable = array_merge($this->fillable, $this->common_fillable);
        $this->hidden = array_merge($this->hidden, $this->common_hidden);
        $this->casts = array_merge($this->casts, $this->common_casts);
        $this->dates = array_merge($this->dates, $this->common_dates);
        $this->appends = array_merge($this->appends, $this->common_appends);
    }

}
