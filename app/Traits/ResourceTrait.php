<?php

namespace App\Traits;

use stdClass;

trait ResourceTrait
{
    private $with_custom = [];

    // not used
    public function newResource($arr=[])
    {
        $resource = new stdClass;

        foreach ($arr as $key=>$val) {
            $resource->$key = $val;
        }

        return $resource;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            'success' => true,
            'message' => 'success',
        ];
    }

    public function withCustom(array $data)
    {
        foreach ($data as $elm) {
            $this->setWith($elm);
        }

        return $this;
    }

    private function setWith($property_name, $is_set=true)
    {
        $prop = "with_{$property_name}";
        $this->{$prop} = $is_set;
        $this->with_custom[str_replace('with_','',$prop)] = $is_set;

        return $this;
    }

    protected function get_property(&$data, $ref, $key)
    {
        $data = array_merge($data, [$key => $ref->{$key}]);
    }

    protected function merge_property(&$data, $new_arr)
    {
        $data = array_merge($data, $new_arr);
    }

}
