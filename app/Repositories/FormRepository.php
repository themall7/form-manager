<?php

namespace App\Repositories;

use App\Repositories\Base\Repository;

class FormRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Form';
    }

    public function getFormsByUserId($user_id)
    {
        return $this->model->where('user_id', $user_id)->get();
    }

    public function createForm($input, $user_id)
    {
        foreach ($input as $row) {
            $this->model->create($input);
        }
        return $this->model->create($input);
    }
}
