<?php
namespace App\Services;

use App\Custom\ErrorResponse;
use App\Custom\MessageResponse;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\BaseResource;
use App\Http\Resources\Form\FormResource;
use App\Models\FormData;
use App\Repositories\FormRepository;
use App\Traits\UtilTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\FormSubmit;

class FormService
{
    use UtilTrait;

    private $form;
    private const user_id = 1;

    // TODO::oauth setup
    private function getUserId()
    {
        return Auth::user()->id;
        return self::user_id;
    }

    public function __construct(FormRepository $form)
    {
        $this->form = $form; //new CodeRepository('App\Models\Form');
    }

    public function getForms($request)
    {
        $user = Auth::user();
        $input = $request->all();

        $forms = $this->form->getFormsByUserId($this->getUserId());
        return new BaseCollection($forms);
    }

    public function getForm($id)
    {
        $user = Auth::user();

        $form = $this->form->find($id);
        if (!$form) {
            return ErrorResponse::noContent(__('messages.common.not_found', ['resource'=>'Form']));
        }

        return (new FormResource($form))->withCustom(['element']);
    }

    public function postForm($request)
    {
        $user = Auth::user();
        $input = $request->all();

        $data = [
            'user_id' => $this->getUserId(),
            'name' => $input['form_name'],
            'element' => serialize($input['element']),
        ];

        $form = $this->form->create($data);
        if (!$form) {
            return ErrorResponse::noContent(__('messages.common.not_found', ['resource'=>'Form']));
        }

        return new BaseResource($form);
    }

    public function putForm($request, $id)
    {
        $user = Auth::user();
        $input = $request->all();

        $data = [
            'user_id' => $this->getUserId(),
            'name' => $input['form_name'],
            'element' => serialize($input['element']),
        ];

        $updated = $this->form->update($data, $id);
        if (!$updated) {
            return ErrorResponse::noContent(__('messages.common.not_found', ['resource'=>'Form']));
        }

        $form = $this->form->find($id);

        return new BaseResource($form);
    }

    public function deleteForm($request, $id)
    {
        $form = $this->form->find($id);

        if (!$form) {
            return ErrorResponse::noContent(__('messages.common.not_found', ['resource'=>'Form']));
        }

        $deleted = $form->delete();

        return MessageResponse::response(__('messages.common.deleted', ['resource'=>'Form']));
    }

    public function postSubmitForm($request, $id)
    {
        $input = $request->all();

        $form = $this->form->find($id);

        if (!$form) {
            return ErrorResponse::noContent(__('messages.common.not_found', ['resource'=>'Form']));
        }

        $inserted = null;
        DB::beginTransaction();
        try {
            $form_submit_id = FormSubmit::insertGetId(['user_id'=>$this->getUserId(), 'form_id'=>$id]);

            $data = [];
            foreach ($input as $key=>$val) {
                $value = $val;
                if (!is_string($val) && get_class($val)==='Illuminate\Http\UploadedFile') {
                    $hash = $this->getHash();
                    $path = "$hash[0]/$hash[1]/$hash[2]";
                    $value = $request->file($key)->store($path);
                }
                $data[] = ['form_submit_id'=>$form_submit_id, 'name'=>$key, 'value'=>$value];
            }
            $inserted = FormData::insert($data);

            DB::commit();
        } catch(\Exception $e) {
            DB::rollback();
            logger($e->getMessage());
        }

        if (!$inserted) {
            return ErrorResponse::noContent(__('messages.common.not_found', ['resource'=>'Form']));
        }

        return MessageResponse::response(__('messages.common.created', ['resource'=>'Form']));
    }
}
