<?php

namespace App\Http\Resources\Form;

use App\Traits\ResourceTrait;
use Illuminate\Http\Resources\Json\JsonResource;
use function Opis\Closure\unserialize;

class FormResource extends JsonResource
{
    use ResourceTrait;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $form = $this->resource;

        foreach ($this->with_custom as $key=>$val) {
            $method = "get_{$key}";
            if ($val) $this->{$method}($form, []);
        }

        return $form;
    }

    private function get_element(&$data, array $ref = [])
    {
        $element = unserialize($data['element']);
        $data['element'] = $element;
    }

}
