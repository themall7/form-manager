<?php

namespace App\Http\Resources;

use App\Traits\ResourceTrait;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BaseCollection extends ResourceCollection
{
    use ResourceTrait;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            'success' => true,
            'message' => 'success',
        ];
    }

    public function withCustom(array $data)
    {
        foreach ($data as $elm) {
            $this->setWith($elm);
        }

        return $this;
    }

    protected function setWith($property_name, $is_set=true)
    {
        $prop = "with_{$property_name}";
        $this->{$prop} = $is_set;
        return $this;
    }

}
