<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class ApiRequest extends FormRequest
{
    /**
     * @param array $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        $error_array = [
            'success' => false,
            'message' => implode(";", $errors),
            'data' => (object)[],
        ];

        return new JsonResponse($error_array, 400);
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $error_msgs = $validator->errors()->messages();
        $error_msgs_ = [];
        foreach ($error_msgs as $key=>$val) {
            $error_msgs_[] = [
                'name' => $key,
                'desc' => is_array($val) ? $val[0] : $val,
            ];
        }

        $validationExceptionClass = [
            'ValidationException' => 'Illuminate\Validation\ValidationException',
            'AppValidationException' => 'App\Exceptions\AppValidationException',
        ];
        $validationException = $validationExceptionClass['ValidationException'];

        throw (new $validationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
    }

}
