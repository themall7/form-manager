<?php

namespace App\Http\Controllers\Api\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\DefaultRequest;
use App\Http\Requests\IDRequest;
use App\Services\FormService;

class FormController extends Controller
{
    private $formService;

    public function __construct(FormService $formService)
    {
        $this->formService = $formService;
    }

    /**
     * @SWG\Get(path="/forms",
     *   tags={FORM},
     *   summary="Get Forms",
     *   description=IMPLEMENTED,
     *   operationId="",
     *   @SWG\Parameter(ref="#/parameters/Authorization_Admin"),
     *   @SWG\Response(
     *      response=200,
     *      description=RESPONSE_200,
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(ref="#/definitions/Form")
     *      )
     *   ),
     *   security={{"app_auth":{}}},
     * )
     */
    public function getForms(DefaultRequest $request)
    {
        return $this->formService->getForms($request);
    }

    /**
     * @SWG\Get(path="/forms/{id}",
     *   tags={FORM},
     *   summary="Get Form Detail",
     *   description=IMPLEMENTED,
     *   operationId="",
     *   @SWG\Parameter(ref="#/parameters/Authorization_Admin"),
     *   @SWG\Parameter(ref="#/parameters/form_id"),
     *   @SWG\Response(response=200, description=RESPONSE_200),
     *   security={{"app_auth":{}}},
     * )
     */
    public function getForm(IDRequest $request, $id)
    {
        return $this->formService->getForm($id);
    }

    /**
     * @SWG\Post(path="/forms",
     *   tags={FORM},
     *   summary="Create a form",
     *   description=IMPLEMENTED,
     *   operationId="",
     *   @SWG\Parameter(ref="#/parameters/Authorization_Admin"),
     *   @SWG\Parameter(
     *     name="form_name",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description="Form name",
     *   ),
     *   @SWG\Parameter(
     *     name="element",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description="Form element",
     *   ),
     *   @SWG\Response(
     *      response=200,
     *      description=RESPONSE_200,
     *   ),
     *   security={{"app_auth":{}}},
     * )
     */
    public function postForm(DefaultRequest $request)
    {
        return $this->formService->postForm($request);
    }

    /**
     * @SWG\Put(path="/forms/{id}",
     *   tags={FORM},
     *   summary="Update a form",
     *   description=IMPLEMENTED,
     *   operationId="",
     *   @SWG\Parameter(ref="#/parameters/Authorization_Admin"),
     *   @SWG\Parameter(
     *     name="form_name",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description="Form name",
     *   ),
     *   @SWG\Parameter(
     *     name="element",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description="Form element",
     *   ),
     *   @SWG\Response(
     *      response=200,
     *      description=RESPONSE_200,
     *   ),
     *   security={{"app_auth":{}}},
     * )
     */
    public function putForm(DefaultRequest $request, $id)
    {
        return $this->formService->putForm($request, $id);
    }

    /**
     * @SWG\Delete(path="/forms/{id}",
     *   tags={FORM},
     *   summary="Delete a form",
     *   description=IMPLEMENTED,
     *   operationId="",
     *   @SWG\Parameter(ref="#/parameters/Authorization_Admin"),
     *   @SWG\Parameter(ref="#/parameters/form_id"),
     *   @SWG\Response(
     *      response=200,
     *      description=RESPONSE_200,
     *   ),
     *   security={{"app_auth":{}}},
     * )
     */
    public function deleteForm(DefaultRequest $request, $id)
    {
        return $this->formService->deleteForm($request, $id);
    }

    /**
     * @SWG\Post(path="/forms/{id}",
     *   tags={FORM},
     *   summary="Submit a form",
     *   description=IMPLEMENTED,
     *   operationId="",
     *   @SWG\Parameter(ref="#/parameters/Authorization"),
     *   @SWG\Response(
     *      response=200,
     *      description=RESPONSE_200,
     *   ),
     *   security={{"app_auth":{}}},
     * )
     */
    public function postSubmitForm(DefaultRequest $request, $id)
    {
        return $this->formService->postSubmitForm($request, $id);
    }

}
