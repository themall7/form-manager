<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DefaultRequest;
use App\Services\FormService;

class FormController extends Controller
{
    private $formService;

    public function __construct(FormService $formService)
    {
        $this->formService = $formService;
    }

    public function viewForms(DefaultRequest $request)
    {
        return view('admin/forms/forms', []);
    }

    public function addForm(DefaultRequest $request)
    {
        return view('admin/forms/add_form', []);
    }

    public function editForm(DefaultRequest $request, $id)
    {
        return view('admin/forms/edit_form', []);
    }

}
