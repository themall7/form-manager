<?php

namespace App\Http\Controllers;

use App\Http\Requests\DefaultRequest;
use App\Services\FormService;

class FormController extends Controller
{
    private $formService;

    public function __construct(FormService $formService)
    {
        $this->formService = $formService;
    }

    public function viewForms(DefaultRequest $request)
    {
        return view('forms/forms', []);
    }

    public function viewForm(DefaultRequest $request)
    {
        return view('forms/rendor_form', []);
    }

}
