<?php

namespace App\Custom;

use Illuminate\Http\JsonResponse;

class ErrorResponse
{
    public static function response($errorMsg, $data=[], $status_code=400) {
        $error = [
            'success' => false,
            'message' => $errorMsg,
        ];
        if (!empty($data)) {
            $error['data'] = $data;
        }

        return new JsonResponse($error, $status_code);
    }

    public static function noContent($errorMsg, $data=[], $status_code=200) {
        $error = [
            'success' => false,
            'message' => $errorMsg,
        ];
        if (!empty($data)) {
            $error['data'] = $data;
        }

        return new JsonResponse($error, $status_code);
    }

}
