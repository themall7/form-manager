<?php

namespace App\Custom;

use stdClass;
use Illuminate\Http\JsonResponse;

class NotFoundResponse
{
    public static function response($errorMsg, $data=null, $status_code=200)
    {
        $error = [
            'success' => false,
            'message' => $errorMsg,
            'data' => $data,
        ];

        if (is_null($data)) {
            $error['data'] = new stdClass();
        }

        return new JsonResponse($error, $status_code);
    }
}
