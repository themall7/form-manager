<?php

namespace App\Custom;

use Illuminate\Http\JsonResponse;

class MessageResponse
{
    public static function response($message) {
        $data = [
            'success' => true,
            'message' => $message,
        ];

        return new JsonResponse($data, 200);
    }
}
