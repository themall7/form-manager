<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      type="object",
 *      required={"id", "name"},
 *      @SWG\Xml(name="Category"),
 *      @SWG\Property(property="id", type="integer"),
 *      @SWG\Property(property="name", type="string"),
 * )
 */
class FormSubmit extends Model
{
    use BaseModelTrait;
    use SoftDeletes;

    protected $table = 'form_submit';
    protected $fillable = ['user_id', 'form_id'];
    protected $hidden = [];
    protected $casts = [];
    protected $dates = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->init();
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function form()
    {
        return $this->belongsTo('App\Model\Form','form_id','id');
    }

    public function formData()
    {
        return $this->hasMany('App\Models\FormData','form_submit_id','id');
    }

}
