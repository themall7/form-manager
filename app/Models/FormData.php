<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      type="object",
 *      required={"id", "name"},
 *      @SWG\Xml(name="Category"),
 *      @SWG\Property(property="id", type="integer"),
 *      @SWG\Property(property="name", type="string"),
 * )
 */
class FormData extends Model
{
    use BaseModelTrait;
    use SoftDeletes;

    protected $table = 'form_data';
    protected $fillable = ['form_submit_id', 'name', 'value'];
    protected $hidden = [];
    protected $casts = [];
    protected $dates = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->init();
    }

    public function formSubmit()
    {
        return $this->belongsTo('App\Model\FormSubmit','form_submit_id','id');
    }

}
