<?php

return [
    'common' => [
        'not_found' => ':resource not found.',
        'deleted' => 'The :resource has been deleted.',
        'created' => 'The :resource has been created.',
        'invalid' => 'Invalid :resource. Please try again.',
    ],
];