@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        Forms<small>Dynamic form generator</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Forms</li>
      </ol>
    </section>
@endsection


@section('content')
<!-- Default box -->
  <div class="row">

    <!-- THE ACTUAL CONTENT -->
    <div class="col-md-12">
      <div class="">

        <div class="row">
          <div class="form-group col-md-12">
            <label id="form_name"></label>
          </div>
        </div>

		<form id="form_rendor" enctype="multipart/form-data">
          <div class="overflow-hidden" id="elm_list">
          </div><!-- /.box-body -->
        </form>

        <div id="saveActions" class="form-group">
          <input type="hidden" name="save_action" value="">
          <div class="btn-group">
            <button type="submit" class="btn btn-success" id="btn_save">
              <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
              <span>Submit</span>
            </button>
          </div>
          <a href="#;" class="btn btn-default" id="btn_cancel"><span class="fa fa-ban"></span> &nbsp;cancel</a>
        </div>

      </div><!-- /.box -->
    </div>

  </div>

@endsection

@section('after_styles')
  <!-- DATA TABLES -->
  <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">

  <!-- CRUD LIST CONTENT - crud_list_styles stack -->
  @stack('crud_list_styles')
@endsection

@section('after_scripts')
  <script src="{{ asset('js/form/render_form.js') }}"></script>

  <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
  @stack('crud_list_scripts')
@endsection

