<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li class="treeview">
    <a href="#"><i class="fa fa-wrench"></i> <span>{{ trans('forms.form_builder') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="{{ backpack_url('forms') }}"> <span>Forms</span></a></li>
      <li><a href="{{ route('add_form') }}"> <span>Add Form</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-cogs"></i> <span>{{ trans('forms.form_generator') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="{{ route('view_forms') }}"> <span>Forms</span></a></li>
    </ul>
</li>