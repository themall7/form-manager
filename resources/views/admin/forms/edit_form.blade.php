@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        Forms<small>Dynamic form builder</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">Forms</li>
      </ol>
    </section>
@endsection


@section('content')
<!-- Default box -->
  <div class="row">

    <!-- THE ACTUAL CONTENT -->
    <div class="col-md-12">
      <div class="">

        <div class="row">
          <div class="form-group col-md-12">
            <label>Form Name</label>
            <input type="hidden" name="form_id" value="" class="form-control" id="form_id">
            <input type="text" name="form_name" value="" class="form-control" id="form_name">
          </div>
        </div>

        <div class="row m-b-10">
          <div class="col-xs-6">
            <div class="hidden-print">
              <a href="#;" class="btn btn-primary ladda-button" data-style="zoom-in" id="btn_text">
                <span class="ladda-label"><i class="fa fa-plus"></i> Text</span>
              </a>
              <a href="#;" class="btn btn-primary ladda-button" data-style="zoom-in" id="btn_textarea">
                <span class="ladda-label"><i class="fa fa-plus"></i> Textarea</span>
              </a>
              <a href="#;" class="btn btn-primary ladda-button" data-style="zoom-in" id="btn_dropdown">
                <span class="ladda-label"><i class="fa fa-plus"></i> Dropdown</span>
              </a>
              <a href="#;" class="btn btn-primary ladda-button" data-style="zoom-in" id="btn_file">
                <span class="ladda-label"><i class="fa fa-plus"></i> File</span>
              </a>
            </div>
          </div>
          <div class="col-xs-6">
              <div id="datatable_search_stack" class="pull-right"></div>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-12">
            <label>Form Elements</label>
            <select class="form-control" id="elm_list">
              <option>--Click button to add an element--</option>
            </select>
          </div>
        </div>

        <div class="overflow-hidden" id="elm_edit">

          <div class="form-group col-md-12">
            <label>Element Type</label>
            <input type="text" name="elm_type" value="" class="form-control" id="elm_type" readonly>
          </div>
          <div class="form-group col-md-12">
            <label>Element Name</label>
            <input type="text" name="elm_name" value="" class="form-control" id="elm_name">
          </div>
          <div class="form-group col-md-12">
            <label>Element Label</label>
            <input type="text" name="elm_label" value="" class="form-control" id="elm_label">
          </div>
          <div class="form-group col-md-12">
            <label>Element Options</label>
            <textarea rows="5" class="form-control" id="elm_options"></textarea>
          </div>

        </div><!-- /.box-body -->

        <div id="saveActions" class="form-group">
          <input type="hidden" name="save_action" value="">
          <div class="btn-group">
            <button type="submit" class="btn btn-success" id="btn_save">
              <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
              <span>Save</span>
            </button>
          </div>
          <a href="#;" class="btn btn-default" id="btn_cancel"><span class="fa fa-ban"></span> &nbsp;cancel</a>
        </div>

      </div><!-- /.box -->
    </div>

  </div>

@endsection

@section('after_styles')
  <!-- DATA TABLES -->
  <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">

  <!-- CRUD LIST CONTENT - crud_list_styles stack -->
  @stack('crud_list_styles')
@endsection

@section('after_scripts')
  <script src="{{ asset('js/admin/form/edit_form.js') }}"></script>

  <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
  @stack('crud_list_scripts')
@endsection

